---
layout: markdown_page
title: "102 - Working at local speed"
---

**Objective:**  Faster website editing: the basics.
Want to be able to quickly make updates to the handbook or about.gitlab.com and not have to wait for every commit to go through a pipeline?   This workshop will show you how to setup git and docker on your local machine and get your work done 200% faster.

## Overview

**Comparison:** In-line edit vs WebIDE vs working locally
In-line editor, for super-quick minor edits


| Approach |  Pros  | Cons |
**WebIDE**  |  - WebIDE is simple to access <br> - WebIDE is awesome <br>- No environment setup <br> - No git commands necessary <br> - Easy to make quick edits <br> - One integrated flow (edit -> MR -> pipeline) | - No advanced git capability <br> - Full pipeline run to see effect of any changes <br> - Limited IDE functionality (compared to tools like Atom) |
| **Working locally** | - Work off-line <br> - Use whatever editor you want <br> - Most minimal feedback loop - see effect of changes immediately (just reload your browser)  |  - Need to remember to push back changes <br> - Need to know git commands <br> - Need to get setup (and maintain) local environment <br> - Need to understand working in a terminal |

### So why do locally?
- faster (tightest feedback loop)

### What does it take to work locally?

Not much:  Simply 3 things
1. a local copy of about.gitlab.com (git does this),
2. an editor or development tool (IDE) - I use Atom,
3. a way to build and run about.gitlab.com (docker)

#### A little terminal time

1. What’s a Terminal:  Simply a way to enter commands into your PC.  To work with Git and Docker, you need to use the Terminal.

1. a few key commands to navigate the terminal
   1. ls, ls -la, cd, cd .., pwd,  
   1. Folders and directories
   1. resources

#### Set up
see the [read.me file from the www-gitlab-com project](https://gitlab.com/gitlab-com/www-gitlab-com?nav_source=navbar)


1. Git - gets and manages your copy of about.gitlab.com
1. It’s time to Git Git
   1. What is Git
   1. Install and Configure Git <Need details>
   1. Configure Git
   1. Set up SHA key
   1. Clone about.gitlab.com <need details>
   1. a few git commands
   1. git pull, git branch, git checkout branch <branch name>
   1. a few git resources

1. Docker: A docker container we call “Middleman” builds and runs about.gitlab.com
   1. What’s docker?
   1. Get Docker:   [Download the Mac version of Docker](https://www.docker.com/get-started)
   1. You will be asked to create a docker account, then download and install docker
   1. Configure Docker to build and run a local copy of about.gitlab.com   IN ONE COMMAND!
   See step 2 for the exact command [read.me file from the www-gitlab-com project](https://gitlab.com/gitlab-com/www-gitlab-com?nav_source=navbar#use-docker-to-render-the-website)
   1. who’s the middleman? - Middleman is the name of the container that you just created.  
   1. a few docker commands
      1. docker start middleman, docker stop middleman
   1. docker resources
1. Get an IDE (such as Atom)  [Download and install Atom](https://atom.io/)

### Working Remotely.  (my workflow)
1. In gitlab.com Create a new branch of about.gitlab.com   call it “name_of_branch”
1. In gitlab.com - create a new MR for the branch I just created
   1. In the MR - click on Checkout Branch and copy the commands
1. Local:   Open terminal
   1. Local:   navigate to the Git directory where about.gitlab.com is stored.   Command: cd ww*   (use the wildecard * so I don’t have to type out ‘www.about.gitlab.com’
   1. Local: Start the website running locally:  in the terminal type: docker start middleman
   - this tells docker to start the container named middleman
   1. Local: in the terminal Paste the checkout commands and hit enter
   - this will refresh your local copy of the project (website) with all the changes
  - and will tell git to point to the branch that you’ve created.
1. Local: Open Atom - the IDE to make edits.
   1. Local: in Atom - make a change to a file
   1. Local: in Atom - save the change
1. Local: in Browser- in the URL window - type: http://localhost:4567/
- This will show you what the Middleman container sees in your local files.  You will instantly (almost) see the changes after you’ve saved your changes.
1. Local: in Atom - make and save more changes.  
1. Local: in Browser - review changes
1. Local: when you’re done.  In Atom - On the ‘git tab’.  
   1. Stage your change
   1. Type a commit message that summarizes your change
   1. Click Commit to commit your changes to your local branch
   - keep working. (making changes, reviewing, committing)... But all of your work is only on your local workstation.
1. When you’re ready to push your changes back to the server.   Click on Push (command in lower right corner of Atom window)
- When you Push, your commits to your branch will be sent to the the Git server.  GitLab will see these changes and kick off a pipeline to build, test and deploy your changes to a review app.
1. In gitlab.com: When the pipeline finishes, review your changes and then have someone Approve your Merge Request.

### Summary:
Git
Docker
Terminal
IDE
