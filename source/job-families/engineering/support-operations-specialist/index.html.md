---
layout: job_family_page
title: "Support Operations Specialist"
---

## Support Operations Specialist

As a Support Operations Specialist, you will be responsible for supporting the day-to-day 
operations and software systems used by GitLab’s global Technical Support team, primarily 
our Zendesk instance, and integrations with internal business processes and tools. 
You will be able to juggle a diverse workload that includes everything from managing 
user provisioning to troubleshooting custom applications in support of a global organization.
As we rely on our systems to support and scale the organization, you will be the backbone and the foundation to our success!

## Responsibilities

- Develop a flexible process and tool execution strategy to support the growth and scalability objectives of GitLab Support and company wide initiatives.
- Own and drive projects to completion in agreement with relevant stakeholders in a cross-departmental function.
- Manage a fast-paced queue of support operations requests, prioritize requests according to business impact, and drive to appropriate completion dates.
- Support and maintain several business critical SaaS systems (e.g. Zendesk Suite, GitLab, Slack, SFDC, Google groups, Zendesk Insights/Explore, and GitLab internal systems) including user administration in accordance with written and audited security controls and configuration changes when needed.
- Develop and maintain system failover processes for customer facing Zendesk apps.
- Provide support and troubleshooting for our Support systems, including App integration, tagging, macros, automations, etc. when necessary.
- Maintain and demonstrate 100% compliance with all written security policies and change management controls and assist with quarterly audits of user access to key systems.
- Understand internal customer's needs and business context to provide outstanding support and maintain high customer satisfaction.
- Assist with implementation of new systems as needed to support evolving processes and critically evaluate our use of systems with a view to improve global efficiency.

## Required Skills/Experience:

- 1-3 years experience in SaaS support with proven ability to support diverse customers needs with skill and humor
- Zendesk Administrator or equivalent enterprise application system administration experience preferred with the ability to gain Zendesk Administrator Certification within 6 months of start date required.
- Demonstrated technical aptitude for, and experience supporting, client-server and/or web-based applications and SaaS/PaaS solutions
- Proven ability to solve practical business problems
- Understanding of business processes and ability to translate business requirements into application functionality
- Zendesk enthusiast who thrives on working in a fast-paced and changing environment
- Strong team player with service-oriented attitude and customer focus
- Excellent written and verbal communication
- BA/BS Degree or equivalent work experience
- An eye for detail and out-of-the-box thinking
- You share our values, and work in accordance with those values
- Successful completion of a background check

## Desired Skills:

- Demonstrated understanding of technical software support processes and concepts
- Experience in CRM or a related industry
- Familiarity with change management processes and risk control compliance
- Experience working on the Zendesk platform as an agent or developer
- Experience with enterprise integration tools 

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Selected candidates receive a short assessment from our Global Recruiters
* Candidates will then be invited to schedule a 45 minute behavioral interview with a Support Engineering Manager
* Candidates will then be invited to schedule a 45 minute behavioral interview with our Director of Support
* Candidates may be invited to schedule an additional 45 minute interview with the VP of Engineering
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).
